//
// Created by Filip Jani on 22.10.16.
//

#ifndef UPS_GUESSERINOSERVER_SERVER_H
#define UPS_GUESSERINOSERVER_SERVER_H

#include <vector>
#include "string"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include "stdio.h"
#include "string.h"
#include "Util.h"
#include "Network.h"
#include "GameRoom.h"
#include "Communication.h"
#include "ServerConfiguration.h"
#include <thread>
#include "Timer.h"

class Server {

public:
    /** Constructor */
    Server(ServerConfiguration::configuration config);
    /** Destructor */
    ~Server();

    /** Starting method */
    void start();
    /** Stop server*/
    void stop();

private:
    /** Players informations */
    std::vector<Util::player_t> players;
    /** Server configuration */
    ServerConfiguration::configuration serverConfig;
    /** Server games */
    std::vector<GameRoom*> gameRooms;
    /** Communication */
    Communication *communication;
    /** Socket */
    struct sockaddr_in addr;

    /** Main server methods */
    void initServer();
    void waitForPlayers();
    static void waitForPlayerResponse(int incommingSocket, Server *server);
    void loginPlayer(int socket, std::string nickName, int freeRoomId);

    /** Help methods */
    bool isNickAvailable(std::string nickName);
    int isLoggedGetRoom(std::string nickName);
    int getFreeRoom();
};


#endif //UPS_GUESSERINOSERVER_SERVER_H
