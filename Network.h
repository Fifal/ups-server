//
// Created by Filip Jani on 19.10.16.
//

#ifndef UPS_GUESSERINOSERVER_NETWORK_H
#define UPS_GUESSERINOSERVER_NETWORK_H


#include <string>

class Network {
public:
    /** Enum of valid used codes */
    enum networkCommands {
        SRV_SUCCESSFULLY_LOGGED, // User was successfully logged
        SRV_NAME_TAKEN, // Name already exists
        SRV_SERVER_FULL, // Server is full
        SRV_IMAGE, // Sending image to client
        SRV_CLIENT_DROPPED, // Client disconnected
        SRV_DRAW, // Client has to draw now
        SRV_WORD, // Word for client to draw
        SRV_INFO,
        SRV_BTNS_GUESS,
        SRV_SCORE,
        SRV_DISCONNECT,
        SRV_BAD_NAME,// Ackn client that server received image
        SRV_ACK,
        CL_CONNECT, // Client is connecting
        CL_DISCONNECT, // Client is disconnecting
        CL_IMAGE, // Client is uploading image
        CL_GUESS, // Client is guessing
        CL_READY, // Client is ready
        CL_ACK,
        NOT_NETWORK_CODE,
        NOT_VALID_MESSAGE
    };

    /** Server message protocol prefix without ':' */
    static std::string PROTOCOL_PREFIX_WO;
    /** Server message protocol prefix */
    static std::string PROTOCOL_PREFIX;
    /** Server message protocol sufix */
    static std::string PROTOCOL_SUFFIX;

    /** Gets string from message code*/
    static std::string getMessage(int messageCode);

    /** Gets network code from string */
    static networkCommands getEnum(std::string message);
};

#endif //UPS_GUESSERINOSERVER_NETWORK_H
