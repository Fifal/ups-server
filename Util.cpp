//
// Created by Filip Jani on 11.10.16.
//
// Class for various useful functions
//

#include <iostream>
#include "Util.h"
#include <fstream>
#include <sstream>

/**
 * Returns vector which contains message splited by delimiter
 * @param str message to split
 * @param delim delimiter
 * @return vector splitted message
 */
std::vector<std::string> Util::split(const std::string str, const std::string delim) {
    {
        std::vector<std::string> tokens;
        size_t prev = 0, pos = 0;
        do {
            pos = str.find(delim, prev);
            if (pos == std::string::npos) pos = str.length();
            std::string token = str.substr(prev, pos - prev);
            if (!token.empty()) tokens.push_back(token);
            prev = pos + delim.length();
        } while (pos < str.length() && prev < str.length());
        return tokens;
    }
}

/**
 * Returns random word from vector which client has to draw
 * @return std::string drawing word
 */
std::string Util::getRandomWord() {
    std::vector<std::string> words = std::vector<std::string>();

    std::string words_string = Util::get_file_content("words.txt");
    if(words_string != ""){
	std::vector<std::string> split = Util::split(words_string, "\n");
	for(auto str : split){
		words.push_back(str);
	}
    }else{
        words.push_back("Prase");
	words.push_back("Pes");
	words.push_back("Auto");
	words.push_back("Strom");
	words.push_back("Slunce");
	words.push_back("Mraky");
	words.push_back("Dům");
	words.push_back("Kočka");
	words.push_back("Telefon");
	words.push_back("Dveře");
	words.push_back("Nůž");
	words.push_back("Lampička");
	words.push_back("Loď");
    }

    int index = getRandomNumber(words.size());

    return words.at(index);

}

/**
 * Gets random number from <0, size>, used for getting random word from vector
 * @param size size of the vector
 * @return int random number
 */
int Util::getRandomNumber(long size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, size);
    return (int) dis(gen);
}

/**
* Gets file content
* @param file_path path to the file
* @return std::string content of the file
*/
std::string Util::get_file_content(std::string file_path) {
    std::ifstream is(file_path);
    if (is.fail()) {
        return "";
    }

    std::stringstream buffer;
    buffer << is.rdbuf();
    return buffer.str();
}
