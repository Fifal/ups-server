//
// Created by Filip Jani on 19.10.16.
//
// Class providing functions for network stuff.
//      - getting network code from string
//      - getting string from network code
//

#include "Network.h"

/** Server protocol message prefix without ':' */
std::string Network::PROTOCOL_PREFIX_WO = "ups";
/** Server protocol message prefix */
std::string Network::PROTOCOL_PREFIX = PROTOCOL_PREFIX_WO + ":";
/** Server protocol message sufix */
std::string Network::PROTOCOL_SUFFIX = ";\n";

/**
 * Gets string from network code
 * @param messageCode network code
 * @return string
 */
std::string Network::getMessage(int messageCode) {
    switch (messageCode) {
        case Network::SRV_SUCCESSFULLY_LOGGED:
            return "srv_connected";
        case Network::SRV_NAME_TAKEN:
            return "srv_nickExists" + PROTOCOL_SUFFIX;
        case Network::SRV_SERVER_FULL:
            return "srv_serverFull" + PROTOCOL_SUFFIX;
        case Network::SRV_CLIENT_DROPPED:
            return "srv_clientDropped";
        case Network::NOT_VALID_MESSAGE:
            return "srv_notValidMessage" + PROTOCOL_SUFFIX;
        case Network::SRV_IMAGE:
            return "srv_image";
        case Network::SRV_DRAW:
            return "srv_draw";
        case Network::SRV_WORD:
            return "srv_word";
        case Network::SRV_INFO:
            return "srv_info";
        case Network::SRV_BTNS_GUESS:
            return "srv_btnsGuess";
        case Network::SRV_SCORE:
            return "srv_score";
        case Network::SRV_DISCONNECT:
            return "srv_disconnect";
        case Network::SRV_BAD_NAME:
            return "srv_badName" + PROTOCOL_SUFFIX;
        case Network::SRV_ACK:
            return "srv_ack";
    }
}

/**
 * Gets network code from string
 * @param message string
 * @return network code
 */
Network::networkCommands Network::getEnum(std::string message) {
    if (message.compare("cl_connect") == 0) {
        return Network::CL_CONNECT;
    } else if (message.compare("cl_disconnect;") == 0) {
        return Network::CL_DISCONNECT;
    } else if (message.compare("cl_guess") == 0) {
        return Network::CL_GUESS;
    } else if (message.compare("cl_image") == 0) {
        return Network::CL_IMAGE;
    } else if (message.compare("cl_ready") == 0) {
        return Network::CL_READY;
    } else if (message.compare("srv_clientDropped") == 0) {
        return Network::SRV_CLIENT_DROPPED;
    } else if (message.compare("srv_notValidMessage") == 0 || message.compare("srv_notValidMessage;") == 0 || message.compare("srv_notValidMessage;\n") == 0 ) {
        return Network::NOT_VALID_MESSAGE;
    } else if (message.compare("cl_ack") == 0) {
        return Network::CL_ACK;
    } else {
        return Network::NOT_NETWORK_CODE;
    }
}
