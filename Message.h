//
// Created by fifal on 17.1.17.
//

#ifndef SERVER_TEST_MESSAGE_H
#define SERVER_TEST_MESSAGE_H


#include <string>
#include "Network.h"

class Message {
private:
    std::string m_text;
    int m_hash_code;
    bool m_confirmed;
    int m_socket;
    Network::networkCommands m_networkCode;
    struct timespec m_sent_time, m_current_time;

    std::string get_send_time();



public:
    Message(int socket, std::string text, Network::networkCommands m_networkCode);

    void confirm(bool m_confirmed);

    int get_hash_code();

    bool is_confirmed();

    int get_socket();

    std::string get_string();

    double get_current_time();

    void update_sent_time();
};


#endif //SERVER_TEST_MESSAGE_H
