//
// Created by fifal on 8.1.17.
//

#ifndef UPS_SERVER_GAMEROOM_H
#define UPS_SERVER_GAMEROOM_H


#include <vector>
#include <thread>
#include "ServerConfiguration.h"
#include "Util.h"
#include "Communication.h"
#include "sys/socket.h"
#include "Timer.h"

class GameRoom {
private:
     //------------------------------------------------------------
     //                      GAME AND PLAYERS                     -
     //------------------------------------------------------------
    int m_ID; /* Game Room ID */
    std::vector<std::shared_ptr<Util::player_t>> m_players; /* Players in room */
    std::string m_guessWord;
    std::string m_img;
    std::mutex m_playersMutex;
    std::string m_drawingPlayerName;
    int m_scoreMax;

    enum GameState{
        WAITING,
        WAITING_FOR_DRAW,
        GUESSING,
        ENDING,
        WAITING_FOR_RECONNECT
    } m_gameState;
    //------------------------------------------------------------

    //------------------------------------------------------------
    //                      GAME ROOM LOOP                       -
    //------------------------------------------------------------
    double m_drawTime_s;
    double m_guessTime_s;
    double m_reconnectTime_s;
    //------------------------------------------------------------

    //------------------------------------------------------------
    //                      SERVER SETTINGS                      -
    //------------------------------------------------------------
    ServerConfiguration::configuration m_serverConfig;

    Communication *m_communication;

    std::thread m_gameRoomThread;
    //------------------------------------------------------------

    //------------------------------------------------------------
    //                           METHODS                         -
    //------------------------------------------------------------
    static void playerListener(int socket, GameRoom *gr); //listener for player
    static void gameRoomLoop(GameRoom *gr); // thread for game loop
    void removePlayer(int socket); // removes player from room


    // Players handling
    void setReady(int socket, bool isReady); // sets player ready
    void setPlayerDrawing(int socket, bool isDrawing); // sets player info that player is drawing

    void setPlayerDrew(int socket, bool hasDrawn); // sets player info that player has draw
    bool getHasPlayerDrawn(int socket);
    bool getHasPlayerDrawn(std::string nickName);

    void increasePlayerScore(int socket, int increment); // increases players score
    void setPlayerGuessed(int socket, bool hasGuessed); // set player info that player guessed

    void setPlayerOnline(int socket, bool isOnline); // sets if player is online
    bool getPlayerOnline(int socket);

    void setPlayerListenerRunning(int socket, bool isRunning);
    int getPlayerIndex(int socket); // returns index of player in m_players vector
    std::shared_ptr<Util::player_t> getPlayerBySocket(int socket);
    std::shared_ptr<Util::player_t> getPlayerByName(std::string nickName);

    // Communication
    void sendInfo(std::string infoMessage); // sends info to clients textarea
    void setButtons(int drawing_player_socket); // sets buttons to all players
    void sendWord(int drawing_player_socket); // sends word to drawing player
    void sendImage(int drawing_player_socket);

    // Game Room Loop
    bool isEveryoneReady(); // checks if everyone is ready
    int isEveryoneOnline(); // checks if everyone is online, should be called in every cycle
    bool isEnoughPlayers(); // checks if there is enough players in room
    void sendResultsAndResetRoom(); // send results to players
    void disconnectPlayers(); // disconnects all players
    void waitForPlayerReconnect(int socket); // waits for player to reconnect
    bool hasEveryoneGuessed(int drawing_player_socket);


    void checkForPlayers();

    bool wait(double time);
    void sleep(int milliseconds);

    int getNextDrawingPlayer();

    //------------------------------------------------------------
public:
    GameRoom(ServerConfiguration::configuration serverConfig, int gameRoomId, Communication * communication);
    ~GameRoom();

    //------------------------------------------------------------
    //                           METHODS                         -
    //------------------------------------------------------------
    void addPlayer(Util::player_t player); // adds player into game room
    void reconnectPlayer(Util::player_t player); // relogs player after connection was lost

    bool isRoomFree(); // returns true if room is free
    bool isNickAvailable(std::string nickName); // returns true if nick is not used in this room
    bool isLoggedButNotOnline(std::string nickName); // returns true if player is in room but not online

    //------------------------------------------------------------
};


#endif //UPS_SERVER_GAMEROOM_H
