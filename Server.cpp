//
// Created by Filip Jani on 22.10.16.
//
// Class provides basic functions for server handling.
//      - Start of the server
//      - Connecting new players
//
#include <unistd.h>
#include "Server.h"

/**
 * Constructor
 * @param config ServerCofiguration - all important server config: port, queue size, max number of games, etc...
 */
Server::Server(ServerConfiguration::configuration config) {
    this->players.resize((unsigned long) (config.maxGames * config.maxPlayersPerGame));
    this->serverConfig = config;
}

/**
 * Starts the server
 */
void Server::start() {
    initServer();
}

/**
 * Initializes server - depending on ServerConfiguration
 *      - Creates vector for GameRooms
 *      - Creates socket for listening
 *      - Binds socket
 *      - Starts WaitForPlayers()
 */
void Server::initServer() {
    this->serverConfig.logger->info("s", "###############################");
    this->serverConfig.logger->info("s", "#      GUESSERINO SERVER      #");
    this->serverConfig.logger->info("s", "###############################\n");
    this->serverConfig.logger->info("sssds", "[initServer]: Spouštím server ", this->serverConfig.ipAddress.c_str(),
                                    ":",
                                    this->serverConfig.serverPort,
                                    ".");

    // Creating of socket
    this->serverConfig.serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    // If given port number is not valid port number -> exit
    if (this->serverConfig.serverPort < 0 || this->serverConfig.serverPort > 65535) {
        this->serverConfig.logger->error("s", "[initServer]: Chyba při vytváření socketu.");
        exit(1);
    }
    int optval = 1;
    setsockopt(this->serverConfig.serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(this->serverConfig.serverPort);

    if (this->serverConfig.ipAddress.compare("localhost") == 0) {
        addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    } else if (this->serverConfig.ipAddress.compare("INADDR_ANY") == 0) {
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
    } else {
        in_addr_t inAddr = inet_addr(this->serverConfig.ipAddress.c_str());
        if (inAddr == -1) {
            serverConfig.logger->error("sss", "[initServer]: Zadaná IP adresa ", serverConfig.ipAddress.c_str(),
                                       " není správná.");
            return;
        } else {
            addr.sin_addr.s_addr = inAddr;
        }
    }

    // Binding of socket
    if (bind(this->serverConfig.serverSocket, (sockaddr *) &addr, sizeof(addr)) < 0) {
        this->serverConfig.logger->error("s", "[initServer]: Chyba při bindování socketu.");
        exit(1);
    }

    // Socket listening
    if (listen(this->serverConfig.serverSocket, this->serverConfig.tcpQueue) < 0) {
        this->serverConfig.logger->error("s", "[initServer]: Chyba při poslouchání socketu.");
        exit(1);
    }

    this->serverConfig.logger->info("s", "[initServer]: Místnosti založeny.");
    this->serverConfig.logger->info("s", "[initServer]: Server spuštěn.");

    // New communication instance for sending/receiving messages
    this->communication = new Communication(this->serverConfig);

    // Initialization of some variables
    this->gameRooms = std::vector<GameRoom *>(this->serverConfig.maxGames);

    for (int j = 0; j < this->serverConfig.maxGames; ++j) {
        this->gameRooms.at(j) = new GameRoom(this->serverConfig, j, communication);
    }

    // While there is any game in progress server will not shut down
    while (true) {
        if (getFreeRoom() != -1) {
            waitForPlayers();
        }
    }

    this->serverConfig.logger->info("s", "[initServer]: Vypínám server.\n");
    exit(1);
}

/**
 * Function which accepts new clients if server is not full
 */
void Server::waitForPlayers() {
    this->serverConfig.logger->info("s", "[waitForPlayers]: Server čeká na připojení hráčů.");

    sockaddr_in incommingAddr;
    unsigned int incommingAddrLength = sizeof(incommingAddr);
    int incommingSocket;

    // While true for accepting new connections
    while (true) {
        incommingSocket = accept(this->serverConfig.serverSocket, (struct sockaddr *) &incommingAddr,
                                 &incommingAddrLength);

        // If there was an error accepting socket -> close socket
        if (incommingSocket < 0) {
            close(incommingSocket);
        }

        // Logs new connection
        this->serverConfig.logger->info("sssdsd", "[waitForPlayers]: Připojení z ", inet_ntoa(incommingAddr.sin_addr),
                                        ":",
                                        ntohs(incommingAddr.sin_port), " socket: ", incommingSocket);

        // Start a new thread which waits for response from player, so it won't get stuck
        std::thread thread(waitForPlayerResponse, incommingSocket, this);
        thread.detach();
    }
}

/**
 * Thread which waits for player's response
 * @param incommingSocket player's socket
 * @param server server instance
 */
void Server::waitForPlayerResponse(int incommingSocket, Server *server) {
    server->serverConfig.logger->info("sds", "[waitForPlayerResponse]: Čekám na odpoveď socketu ", incommingSocket,
                                      ".");

    // Gets new message from client
    std::string message = server->communication->receiveMessage(incommingSocket);
    // Splits message into array by ":"
    std::vector<std::string> splittedMessage = Util::split(message, ":");

    int freeRoomId;

    if ((splittedMessage[0].compare("ups") == 0) && splittedMessage.size() > 1) {
        // If message code is anything but cl_connect -> close socket
        switch (Network::getEnum(splittedMessage[1])) {
            case Network::CL_CONNECT: {
                if (splittedMessage.size() == 4 && splittedMessage[2].length() <= 15 &&
                    splittedMessage[2].length() > 1) {

                    server->communication->sendMessage(incommingSocket, Network::PROTOCOL_PREFIX +
                                                                        Network::getMessage(Network::SRV_ACK) + ":" +
                                                                        splittedMessage[3] +
                                                                        Network::PROTOCOL_SUFFIX);

                    freeRoomId = server->getFreeRoom();
                    if (freeRoomId != -1) {
                        server->loginPlayer(incommingSocket, splittedMessage[2], freeRoomId);
                    } else if (server->isLoggedGetRoom(splittedMessage[2]) != -1) {
                        server->loginPlayer(incommingSocket, splittedMessage[2], freeRoomId);
                    } else {
                        std::string full_msg = Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_SERVER_FULL);
                        server->communication->sendMessage(incommingSocket, full_msg);
                        close(incommingSocket);
                    }
                } else {
                    std::string full_msg = Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_BAD_NAME);
                    server->communication->sendMessage(incommingSocket, full_msg);
                    close(incommingSocket);
                }
                break;
            }
            case Network::SRV_CLIENT_DROPPED: {
                server->serverConfig.logger->info("sds", "[waitForPlayerResponse]: Spojení se socketem ",
                                                  incommingSocket, " ztraceno.");
                close(incommingSocket);
                break;
            }
            default: {
                close(incommingSocket);
                break;
            }
        }
    }
    server->serverConfig.logger->info("sds", "[waitForPlayerResponse]: Konec čekání na odpoveď socketu ",
                                      incommingSocket,
                                      ".");
    server->serverConfig.logger->debug("sds", "[waitForPlayerResponse]: Vlákno pro socket ", incommingSocket,
                                       " ukončeno.");
}

/**
 * Logins player into a server
 * @param socket players socket
 * @param nickName players nickname
 */
void Server::loginPlayer(int socket, std::string nickName, int freeRoomId) {
    if (nickName.length() > 25) {
        nickName.resize(25);
    }
    // If chosen NickName is available
    if (isNickAvailable(nickName)) {
        Util::player_t player;

        player.nickName = nickName;
        player.socket = socket;

        // If there is any free room, add player into this room and send message to client that he has been added
        if (freeRoomId != -1) {
            this->gameRooms.at(freeRoomId)->addPlayer(std::move(player));
            this->serverConfig.logger->info("sssd", "[loginPlayer]: Uživatel ", nickName.c_str(),
                                            " byl připojen do místnosti id ",
                                            freeRoomId);
            Message msg(socket, "", Network::SRV_SUCCESSFULLY_LOGGED);
            communication->addMessage(msg);
        }
    } else if (isLoggedGetRoom(nickName) != -1) {
        this->serverConfig.logger->debug("s", "[loginPlayer]: Uživatel je již v místnosti, bude přelogován. ");
        Util::player_t player;
        player.nickName = nickName;
        player.socket = socket;

        this->gameRooms.at(isLoggedGetRoom(nickName))->reconnectPlayer(std::move(player));
    } else {
        // Logs info that NickName already exists
        this->serverConfig.logger->info("s",
                                        "[loginPlayer]: Uživatel se zadaným nickem již existuje, odpojuji uživatele.");

        // Sends message to client that NickName is not available
        std::string message = Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_NAME_TAKEN);
        this->communication->sendMessage(socket, message);
        close(socket);
    }
}

/**
 * Checks if user nickname is available
 * @param nickName string nickname
 * @return true if nickname is available
 */
bool Server::isNickAvailable(std::string nickName) {
    // Checks all rooms if NickName exists
    for (int i = 0; i < this->gameRooms.size(); ++i) {
        if (!gameRooms.at(i)->isNickAvailable(nickName)) {
            return false;
        }
    }
    return true;
}

/**
 * Checks if user with nickname is logged but dropped
 * @param nickName string nickname
 * @return true if is logged but not online
 */
int Server::isLoggedGetRoom(std::string nickName) {
    // Check all rooms if nicknameExists  && if is online
    for (int i = 0; i < this->gameRooms.size(); ++i) {
        if (gameRooms.at(i)->isLoggedButNotOnline(nickName)) {
            return i;
        }
    }
    return -1;

}

/**
 * Returns ID of the first free room
 * @return int index of the first room
 */
int Server::getFreeRoom() {
    int freeRoomId = -1;
    for (int i = 0; i < this->gameRooms.size(); ++i) {
        if (this->gameRooms.at(i)->isRoomFree()) {
            freeRoomId = i;
            break;
        }
    }
    return freeRoomId;
}

/**
 * Destructor
 */
Server::~Server() {
    for (int i = 0; i < this->gameRooms.size(); ++i) {
        delete (this->gameRooms.at(i));
    }
    delete (this->communication);

    this->serverConfig.logger->info("s", "Vypínám server.");
    this->serverConfig.logger->info("s", "###############################\n");
}