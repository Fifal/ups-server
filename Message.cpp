//
// Created by fifal on 17.1.17.
//

#include "Message.h"
Message::Message(int socket, std::string text, Network::networkCommands networkCode){
    if(networkCode == Network::SRV_INFO){
        m_text = get_send_time() + text;
    }else{
        m_text = text;
    }
    m_confirmed = false;
    m_hash_code = std::hash<std::string>{}(Network::getMessage(networkCode) + text + std::to_string(rand()));
    m_socket = socket;
    m_networkCode = networkCode;
    clock_gettime(CLOCK_MONOTONIC, &m_sent_time);
}

void Message::confirm(bool m_confirmed) {
    Message::m_confirmed = m_confirmed;
}

int Message::get_hash_code(){
    return m_hash_code;
}

bool Message::is_confirmed(){
    return m_confirmed;
}

int Message::get_socket(){
    return m_socket;
}


std::string Message::get_string(){
    if(m_text == "") {
        return Network::PROTOCOL_PREFIX + Network::getMessage(m_networkCode) + ":" + std::to_string(m_hash_code) +Network::PROTOCOL_SUFFIX;
    }else{
        return Network::PROTOCOL_PREFIX + Network::getMessage(m_networkCode) + ":" + m_text + ":" + std::to_string(m_hash_code) + Network::PROTOCOL_SUFFIX;
    }
}

std::string Message::get_send_time() {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "[%H><%M><%S]>< ", timeinfo);
    std::string str(buffer);
    return str;
}

double Message::get_current_time() {
    clock_gettime(CLOCK_MONOTONIC, &m_current_time);
    double elapsed = 0;
    elapsed = (m_current_time.tv_sec - m_sent_time.tv_sec);
    elapsed += (m_current_time.tv_nsec - m_sent_time.tv_nsec) / 1000000000.0;
    return elapsed;
}

void Message::update_sent_time() {
    clock_gettime(CLOCK_MONOTONIC, &m_sent_time);
}
