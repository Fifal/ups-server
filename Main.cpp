#include <iostream>
#include "Server.h"
#include "Debug.h"

const int SERVER_PORT = 20001;
const int TCP_QUEUE = 10;
const int MAX_PLAYERS = 2;
const int MAX_GAMES = 1;
/** Maximal lenght of TCP message */
const int MAX_TXT_LENGHT = 32768;

void consoleListener();
void printHelp();

Server *server;
Logger *logger;

int main(int argc, char *argv[]) {
    Debug::DEBUG = true;

    if (Debug::DEBUG) {
        logger = new Logger("server.log", true);
        ServerConfiguration *serverConfiguration = new ServerConfiguration("31.31.77.181", SERVER_PORT, TCP_QUEUE, MAX_PLAYERS,
                                                                           MAX_GAMES, MAX_TXT_LENGHT, logger);

        std::thread consoleThread(consoleListener);
        consoleThread.detach();

        server = new Server(serverConfiguration->config);
        server->start();

        delete(logger);
        delete(server);
        delete(serverConfiguration);
    } else {
        if (argc == 6) {
            logger = new Logger(argv[5], true);

            std::thread consoleThread(consoleListener);
            consoleThread.detach();

            // PORT
            if(std::stoi(argv[2]) < 0 || std::stoi(argv[2]) > 65535){
                logger->error("s", "Nastaven špatný port, povolené hodnoty jsou <0; 65535>");
                printHelp();
                return 1;
            }

            // MAX GAMES
            if(std::stoi(argv[3]) > 10 || std::stoi(argv[3]) < 1){
                logger->error("s", "Nastaven špatný počet her, povolené hodnoty jsou <1; 10>");
                printHelp();
                return 1;
            }

            // MAX PLAYERS PER GAME
            if(std::stoi(argv[4]) < 2 || std::stoi(argv[4]) > 4){
                logger->error("s", "Nastaven špatný počet hráčů, povolené hodnoty jsou <2; 4>");
                printHelp();
                return 1;
            }

            ServerConfiguration *serverConfiguration = new ServerConfiguration(argv[1], atoi(argv[2]), TCP_QUEUE, atoi(argv[4]),
                                                                               atoi(argv[3]), MAX_TXT_LENGHT, logger);
            server = new Server(serverConfiguration->config);
            server->start();

            delete(logger);
            delete(server);
            delete(serverConfiguration);
        } else {
            printHelp();
        }
    }
}

void printHelp(){
    std::cout << "Použití: ./server <IP_ADDR> <PORT> <MAX_GAMES> <MAX_PLAYERS> <LOGGER_FILE>\n" << std::endl;
    std::cout << "IP_ADDR \t- adresa na které bude server poslouchat - možno \"INADDR_ANY\", \n\t\t\"localhost\" nebo IP adresa" << std::endl;
    std::cout << "PORT \t\t- port na kterém bude aplikace poslouchat" << std::endl;
    std::cout << "MAX_GAMES \t- maximální počet her na serveru" << std::endl;
    std::cout << "MAX_PLAYERS \t- maximální počet hráčů ve hře" << std::endl;
    std::cout << "LOGGER_FILE \t- název logovacího souboru" << std::endl << std::endl;
    std::cout << "Například: ./server INADDR_ANY 20001 4 4 server.log" << std::endl;
    std::cout
            << "Nabinduje socket na všechny interface na portu 20001, na server budou 4 hry pro maximálně 4 hráče a bude se logovat do souboru server.log"
            << std::endl << std::endl;
}

/**
 * Listener for console commands
 */
void consoleListener(){
    while(1){
        int ch = getchar();
        switch (ch){
            case 'x': {
                exit(0);
                break;
            }
            case 'd': {
                Debug::DEBUG = !Debug::DEBUG;
                logger->info("sss", "Debug nastaven na ", std::to_string(Debug::DEBUG).c_str(), ".");
                break;
            }
        }
	std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}
